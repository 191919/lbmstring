//
// A minimalist std::string replacement
//
// Copyright (c) 2012 jh <`perl -e 'print 19*10000+19*100+19'` chr(64) gmail %2f com>.
// All rights reserved.
//
// Revision 0.0.2012.03.09
// Revision 0.0.2013.09.18
//
// (0) Its original purpose is to be a drop-in replacement of the excessively used
//     std::string in crtmpd instead of a general-purpose string class. Do not expect much.
// (1) For small strings with no-more-than CAP characters, all ops are done in stack.
// (2) For large strings, make things just work, but in suboptimal ways. malloc and realloc
//     may be called many times if you alter the string frequently.
// (3) To retrieve maximum performance, use alternative malloc library, e.g., tcmalloc.
// (4) Don't use when sizeof(lbmstring) matters.
// (5) Read notes very seriously before you decide to use LBM_STATIC_STR feature.
// (6) Special care has been taken account for the use with std::tr1::unordered_map/set
//     and boost::unordered_map/set. The hash function is classical DJB hash.
//
// Distributed under GPLv3.
//

#ifndef _lbmstring_h
#define _lbmstring_h

#include <string>
#include <iostream>
#include <assert.h>

// You can use this macro to test if your program is dying.
#define ASSERT_STATIC_STR(s) assert((sizeof(s)-1) == strlen(s))

#define LBM_STATIC_STR(s) std::lbmstring((s), (sizeof(s)-1), NULL)

// If you are constructing a lbmstring with an allocated-on-heap string, this
// macro will make your program die very fast.
#define LBM_STATIC_STR_SAFE(s) std::lbmstring((((sizeof(s)-1) == strlen(s)) ? (s) : NULL), (sizeof(s)-1), NULL)

namespace std
{

class lbmstring
{
private:
	enum { CAP = 128 };

	char sbuf_[CAP];
	char* mbuf_;
	char* buf_;
	size_t len_;
	int policy_;

	void _assign_buf(const char* s, size_t n)
	{
		len_ = n;

		_realloc_mem(len_);
		memcpy(buf_, s, len_);
		buf_[len_] = 0;
	}

	void _realloc_mem(size_t n)
	{
		if (policy_ == P_MALLOC) free(mbuf_);

		len_ = n;
		if (n >= CAP)
		{
			policy_ = P_MALLOC;
			buf_ = mbuf_ = (char*) malloc(n + 1);
		}
		else
		{
			policy_ = P_STACK;
			buf_ = sbuf_;
		}
	}

public:
	typedef size_t size_type;
	typedef char value_type;
	typedef char* iterator;
	typedef const char* const_iterator;

	enum { npos = (size_type) (-1) };
	enum { P_NULL = -1, P_STACK = 0, P_MALLOC = 1, P_STATIC = 2 };

	~lbmstring()
	{
		if (policy_ == P_MALLOC) free(mbuf_);
	}

	lbmstring() : buf_(sbuf_), len_(0), policy_(P_STACK)
	{
		buf_[0] = 0;
	}

	lbmstring(const std::string& s) : policy_(P_STACK)
	{
		(void) operator=(s.c_str());
	}

	lbmstring(const char* s) : policy_(P_STACK)
	{
		(void) operator=(s);
	}

	lbmstring(const lbmstring& ls) : policy_(P_STACK)
	{
		(void) operator=(ls);
	}

	lbmstring(const char* s, size_t n) : policy_(P_STACK)
	{
		_assign_buf(s, n);
	}

	//
	// NOTE
	// The only circumstance under which you should use this form of constructor is
	// when you want a lbmstring whose internal buffer is allocated in a read-only
	// segment inside the final executable.
	//
	// E.g., lbmstring("aaa", 3, NULL) where "aaa" is in .rodata segment.
	//	
	// It is for a very special purpose. Bear in mind that it makes the string
	// totally read-only.
	//
	lbmstring(const char* s, size_t n, void********) : mbuf_(NULL), buf_((char*) s), len_(n), policy_(P_STATIC)
	{
	}

	lbmstring(size_t n, char c) : len_(n), policy_(P_STACK)
	{
		_realloc_mem(len_);
		memset(buf_, c, len_);
		buf_[len_] = 0;
	}

	void attach(const char* b)
	{
		attach(b, strlen(b));
	}

	// Don't use it on stack-allocated buffers
	void attach(const char* b, size_t n)
	{
		if (policy_ == P_MALLOC) free(mbuf_);

		mbuf_ = buf_ = b;
		len_ = n;
		policy = P_MALLOC;
	}

	int policy() const { return policy_; }
	size_t length() const { return len_; }
	size_t size() const { return len_; }
	const char* c_str() const { return (const char*) buf_; }

	size_t find(const lbmstring& ls, size_t pos = 0) const
	{
		const char* p = strstr(buf_ + pos, ls.buf_);
		if (p == NULL) return npos;
		return p - buf_;
	}

	size_t find(const char c, size_t pos = 0) const
	{
		const char* p = strchr(buf_ + pos, c);
		if (p == NULL) return npos;
		return p - buf_;
	}

	size_t rfind(const char c, size_t pos = 0) const
	{
		const char* p = strrchr(buf_ + pos, c);
		if (p == NULL) return npos;
		return p - buf_;
	}

	size_t find_last_of(const char* s, size_t pos, size_t n) const
	{
		if (len_ == 0) return npos;

		for (size_t i = pos; i != 0 && n != 0; --i, --n)
		{
			if (strchr(s, buf_[i])) return i;
		}
		return npos;
	}

	size_t find_last_of(const lbmstring& ls) const
	{
		return find_last_of(ls.buf_, len_ - 1, len_);
	}

	size_t find_last_of(const char c) const
	{
		return rfind(c);
	}

	lbmstring& replace(size_t pos, size_t n1, const char* s, size_t n2)
	{
		assert(policy_ != P_STATIC);

		size_t newlen = len_ + n2 - n1;
		char tmp[CAP];
		char *newbuf;
		bool free_old = (policy_ == P_MALLOC);
		if (newlen >= CAP)
		{
			policy_ = P_MALLOC;
			newbuf = (char*) malloc(newlen + 1);
		}
		else
		{
			policy_ = P_STACK;
			newbuf = tmp;
		}

		memcpy(newbuf, buf_, pos);
		memcpy(newbuf + pos, s, n2);
		memcpy(newbuf + pos + n2, buf_ + pos + n1, len_ - pos - n1);

		len_ = newlen;
		newbuf[len_] = 0;

		if (free_old) free(mbuf_);

		if (policy_ == P_STACK)
		{
			memcpy(sbuf_, newbuf, len_ + 1);
			buf_ = sbuf_;
		}
		else
		{
			buf_ = mbuf_ = newbuf;
		}

		return *this;
	}

	lbmstring& replace(size_t pos, size_t n1, const lbmstring& ls)
	{
		return replace(pos, n1, ls.buf_, ls.len_);
	}

	lbmstring substr(size_t begin, size_t len = npos) const
	{
		return lbmstring(buf_ + begin, (len == npos) ? len_ - begin : len);
	}

	lbmstring& operator=(const char* s)
	{
		_assign_buf(s, strlen(s));
		return *this;
	}

	lbmstring& operator=(const lbmstring& ls)
	{
		if (ls.policy_ == P_STATIC)
		{
			if (policy_ == P_MALLOC && mbuf_ != NULL) free(mbuf_);

			policy_ = P_STATIC;

			buf_ = ls.buf_;
			len_ = ls.len_;
			mbuf_ = NULL;
		}
		else
		{
			_assign_buf(ls.buf_, ls.len_);
		}
		return *this;
	}

#if (__cplusplus >= 201103L)
	lbmstring& operator=(const lbmstring&& ls)
	{
		policy = ls.policy_;
		buf_ = ls.buf_;
		mbuf_ = ls.buf_;
		len_ = ls.buf_;

		ls.policy = P_NULL;
		ls.buf_ = NULL;
		ls.mbuf_ = NULL;
		ls.len_ = 0;
	}
#endif

	char& operator[](int i)
	{
		return buf_[i];
	}

	lbmstring& append(const char* s, size_t l)
	{
		assert(policy_ != P_STATIC);

		if (len_ + l >= CAP)
		{
			if (policy_ == P_STACK)
			{
				char* tmp = (char*) malloc(len_ + l + 1);
				memcpy(tmp, buf_, len_);
				buf_ = mbuf_ = tmp;
			}
			else
			{
				buf_ = mbuf_ = (char*) realloc(mbuf_, len_ + l + 1);
			}
			policy_ = P_MALLOC;
		}
		memcpy(&buf_[len_], s, l);
		len_ += l;
		buf_[len_] = 0;
		return *this;
	}

	lbmstring& append(const char c)
	{
		return append(&c, 1);
	}

	lbmstring& append(const char* s)
	{
		return append(s, strlen(s));
	}

	lbmstring& operator+=(const char* s)
	{
		return append(s);
	}

	lbmstring& operator+=(const char c)
	{
		return append(&c, 1);
	}

	lbmstring& operator+=(const lbmstring& ls)
	{
		return append(ls.buf_, ls.len_);
	}

	bool operator==(const lbmstring& ls) const
	{
		if (buf_ == ls.buf_) return true;
		return strcmp(buf_, ls.buf_) == 0;
	}

	bool operator==(const char* s) const
	{
		if (buf_ == s) return true;
		return strcmp(buf_, s) == 0;
	}

	bool operator!=(const lbmstring& ls) const
	{
		if (buf_ == ls.buf_) return false;
		return strcmp(buf_, ls.buf_) != 0;
	}

	bool operator!=(const char* s) const
	{
		if (buf_ == s) return false;
		return strcmp(buf_, s) != 0;
	}

	bool operator<(const lbmstring& ls) const
	{
		return strcmp(buf_, ls.buf_) < 0;
	}

	bool operator>(const lbmstring& ls) const
	{
		return strcmp(buf_, ls.buf_) > 0;
	}

	// for completion
	iterator begin() const { return buf_; }
	iterator end() const { return buf_ + len_; }
	iterator rbegin() const { return end() - 1; }
	iterator rend() const { return begin() - 1; }

}; // class lbmstring

// I really don't have any clue of why this brings approx. 5% performance boost
template<>
class less<std::lbmstring>
{
public:
	bool operator()(const std::lbmstring& a, const std::lbmstring& b) const
	{
		return strcmp(a.c_str(), b.c_str()) < 0;
	}
};

inline uint64_t lbm_hash_djb2(const char *str, size_t length)
{
    uint64_t hash = 5381LL;
    while (length--)
	{
        hash = ((hash << 5) + hash) + *str++;
    }
    return hash;
}

namespace tr1
{

// do not include <tr1/functional> which increases 1500% compilation time on Intel C++ Compiler
template<typename T> struct hash;

template <>
struct hash<lbmstring> : public unary_function<lbmstring, size_t>
{
   size_t operator()(const lbmstring& s) const
   {
	   return (size_t) lbm_hash_djb2(s.c_str(), s.length());//hash_len);
   }
};

} // namespace tr1

} // namespace std

namespace boost
{

// for boost's std::tr1::unordered_map connectivity
inline std::size_t hash_value(const std::lbmstring& s)
{
	return (size_t) std::lbm_hash_djb2(s.c_str(), s.length());
}

} // namespace boost

inline std::ostream& operator<<(std::ostream& os, const std::lbmstring& ls)
{
	return os.write(ls.c_str(), ls.length());
}

inline std::lbmstring operator+(const std::lbmstring& l, const std::lbmstring& r)
{
	std::lbmstring p(l);
	p += r;
	return p;
}

inline std::lbmstring operator+(const char l, const std::lbmstring& r)
{
	std::lbmstring p(1, l);
	p += r;
	return p;
}

#endif // _lbmstring_h
